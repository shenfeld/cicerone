package com.shenfeld.cicerone.ui.activities.mainActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shenfeld.cicerone.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()
    private val cicerone: Cicerone<Router> by inject()
    private val navigator = SupportAppNavigator(this, R.id.clDataContainer)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    private fun setupView() {
        viewModel.onForwardCommandRecentsScreen()
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_recents -> viewModel.onForwardCommandRecentsScreen()
                R.id.action_favorites -> viewModel.onForwardCommandFavoritesScreen()
                R.id.action_nearby -> viewModel.onForwardCommandNearbyScreen()
            }
            true
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        cicerone.navigatorHolder.removeNavigator()
    }
}