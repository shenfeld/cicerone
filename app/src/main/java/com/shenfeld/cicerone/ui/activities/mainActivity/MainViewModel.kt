package com.shenfeld.cicerone.ui.activities.mainActivity

import androidx.lifecycle.ViewModel
import com.shenfeld.cicerone.navigation.FavoritesScreen
import com.shenfeld.cicerone.navigation.NearbyScreen
import com.shenfeld.cicerone.navigation.RecentsScreen
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class MainViewModel : ViewModel(), KoinComponent {
    private val cicerone: Cicerone<Router> by inject()
    private val router = cicerone.router

    fun onBackCommandClick() {
        router.exit()
    }

    fun onForwardCommandRecentsScreen() {
        router.navigateTo(RecentsScreen())
    }

    fun onForwardCommandFavoritesScreen() {
        router.navigateTo(FavoritesScreen())
    }

    fun onForwardCommandNearbyScreen() {
        router.navigateTo(NearbyScreen())
    }
}