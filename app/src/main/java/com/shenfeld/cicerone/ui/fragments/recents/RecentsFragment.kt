package com.shenfeld.cicerone.ui.fragments.recents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.shenfeld.cicerone.R
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

@Suppress("UNREACHABLE_CODE")
class RecentsFragment : Fragment() {
    private val viewModel: RecentsFragmentViewModel by viewModel()
    private val cicerone: Cicerone<Router> by inject()

    companion object {
        fun newInstance() = RecentsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recents, container, false)
    }
}