package com.shenfeld.cicerone.ui.fragments.nearby

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.shenfeld.cicerone.R
import com.shenfeld.cicerone.ui.fragments.recents.RecentsFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class NearbyFragment : Fragment() {
    private val viewModel: RecentsFragmentViewModel by viewModel()
    private val cicerone: Cicerone<Router> by inject()

    companion object {
        fun newInstance() = NearbyFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_nearby, container, false)
    }
}