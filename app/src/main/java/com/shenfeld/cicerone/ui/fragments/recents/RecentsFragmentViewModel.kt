package com.shenfeld.cicerone.ui.fragments.recents

import androidx.lifecycle.ViewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class RecentsFragmentViewModel : ViewModel(), KoinComponent {
    private val cicerone: Cicerone<Router> by inject()
    private val router = cicerone.router

    fun onBackCommandClick() {
        router.exit()
    }
}