package com.shenfeld.cicerone.navigation

import androidx.fragment.app.Fragment
import com.shenfeld.cicerone.ui.fragments.nearby.NearbyFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class NearbyScreen : SupportAppScreen() {
    override fun getScreenKey(): String {
        return super.getScreenKey()
    }

    override fun getFragment(): Fragment? {
        return NearbyFragment.newInstance()
    }
}