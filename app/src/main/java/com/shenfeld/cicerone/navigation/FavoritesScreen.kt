package com.shenfeld.cicerone.navigation

import androidx.fragment.app.Fragment
import com.shenfeld.cicerone.ui.fragments.favorites.FavoritesFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class FavoritesScreen : SupportAppScreen() {
    override fun getScreenKey(): String {
        return super.getScreenKey()
    }

    override fun getFragment(): Fragment? {
        return FavoritesFragment.newInstance()
    }
}