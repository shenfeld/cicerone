package com.shenfeld.cicerone.navigation

import androidx.fragment.app.Fragment
import com.shenfeld.cicerone.ui.fragments.recents.RecentsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class RecentsScreen : SupportAppScreen() {
    override fun getScreenKey(): String {
        return super.getScreenKey()
    }

    override fun getFragment(): Fragment? {
        return RecentsFragment.newInstance()
    }
}