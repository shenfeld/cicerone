package com.shenfeld.cicerone.di

import com.shenfeld.cicerone.ui.fragments.favorites.FavoritesFragmentViewModel
import com.shenfeld.cicerone.ui.activities.mainActivity.MainViewModel
import com.shenfeld.cicerone.ui.fragments.nearby.NearbyFragmentViewModel
import com.shenfeld.cicerone.ui.fragments.recents.RecentsFragmentViewModel
import com.shenfeld.cicerone.utils.ResourcesManager
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone

val presentationModule = module {

    viewModel { MainViewModel() }
    viewModel { RecentsFragmentViewModel() }
    viewModel { FavoritesFragmentViewModel() }
    viewModel { NearbyFragmentViewModel() }

    single { Cicerone.create() }
    single { ResourcesManager(androidContext()) }

}